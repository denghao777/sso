package sso

import (
	"context"
	"errors"
	"gitee.com/denghao777/sso/model"
	log "github.com/sirupsen/logrus"

	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

func JwtHandlerFunc() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.GetHeader("X-Auth-Request-Access-Token")
		if token == "" {
			token = c.GetHeader("X-Access-Token")
		}

		if userContext, err := ParseToken(token); err == nil {
			if ok, err := GetOpa().Eval(Input{
				Method: c.Request.Method,
				Path:   c.Request.URL.Path,
				Roles:  userContext.Roles,
			}); err != nil || !ok {
				if err != nil {
					log.Errorf("Permission verification failure : %s", err.Error())
				}
				errorResp(c, "Insufficient authority")
				c.Abort()
				return
			}

			ctx := context.WithValue(c.Request.Context(), "userContext", userContext)
			c.Request = c.Request.WithContext(ctx)
			c.Next()
		} else {
			errorResp(c, "failed to parse the token")
			c.Abort()
			return
		}
	}
}

func ParseToken(token string) (*UserContext, error) {
	tokenClaims, err := model.JwtDecode(token)
	if err != nil {
		return nil, err
	}

	payload := tokenClaims.Payload
	username := payload["user_name"].(string)
	tenant := payload["tenant"].(string)
	authorities := payload["authorities"].([]interface{})

	if username == "" || tenant == "" || len(authorities) <= 0 {
		return nil, errors.New("invalid token")
	}

	roles := make([]string, len(authorities))
	for index, value := range authorities {
		roles[index] = value.(string)
	}

	user := new(UserContext)
	user.Token = token
	user.Username = username
	user.Tenant = tenant
	user.Roles = roles

	return user, nil
}

func IsExpiration(token string) (bool, error) {
	jwt, err := model.JwtDecode(token)
	if err != nil {
		return false, err
	}

	payload := jwt.Payload
	exp := int64(payload["exp"].(float64))
	if exp <= time.Now().Unix() {
		return true, nil
	}
	return false, nil
}

func errorResp(c *gin.Context, msg string) {
	c.JSON(http.StatusUnauthorized, model.Resp{
		Code:      http.StatusUnauthorized,
		Message:   msg,
		Timestamp: time.Now().UnixMilli(),
	})
}
