package sso

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type Param struct {
	Url    string
	Query  map[string]string
	Header map[string]string
	Body   string
}

type Res struct {
	StatusCode int
	Body       string
}

const (
	GET    = "GET"
	POST   = "POST"
	PUT    = "PUT"
	DELETE = "DELETE"
)

// Client和Transport类型都可以安全的被多个go程同时使用。出于效率考虑，应该一次建立、尽量重用。
var client = &http.Client{
	Timeout: 30 * time.Second,
}

func httpExecute(httpMethod string, param *Param) (*Res, error) {
	bT := time.Now()
	urlStr, err := BuildUrl(param.Url, param.Query)
	if err != nil {
		return nil, err
	}

	body := param.Body
	var payload strings.Reader
	if body != "" {
		temp := strings.NewReader(body)
		payload = *temp
	}

	hm, flag := checkHttpMethod(httpMethod)
	if !flag {
		return nil, errors.New("Invalid HttpMethod :" + hm)
	}

	//request
	request, err := http.NewRequest(hm, urlStr, &payload)
	log.Debugf("requestMethod : %s , requestPath : %s , requestBody : %s ", httpMethod, urlStr, body)

	header := param.Header
	if header != nil {
		for key, value := range header {
			request.Header.Add(key, value)
		}
	}

	if strings.EqualFold(hm, POST) || strings.EqualFold(hm, PUT) {
		request.Header.Add("Content-Type", "application/json")
	}

	resp, err := client.Do(request)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	result, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	eT := time.Since(bT)
	log.Debugf("request : %s , time cost = %v , response body : %s ", urlStr, eT, string(result))

	return &Res{
		StatusCode: resp.StatusCode,
		Body:       string(result),
	}, err
}

// verify httpMethod , currently supports POST,GET,PUT,DELETE
func checkHttpMethod(httpMethod string) (string, bool) {
	switch strings.ToUpper(httpMethod) {
	case GET:
		return GET, true
	case POST:
		return POST, true
	case PUT:
		return PUT, true
	case DELETE:
		return DELETE, true
	}
	return httpMethod, false

}

// build url to "http://localhost:9999/test?A=1&B=3"
func BuildUrl(urlStr string, query map[string]string) (string, error) {
	if urlStr == "" {
		return "", errors.New("url cannot be empty!")
	}

	if query != nil {
		data := url.Values{}
		for key, value := range query {
			data.Set(key, value)
		}
		u, err := url.ParseRequestURI(urlStr)
		if err != nil {
			log.Errorf("parse url requestUrl failed,err:%v\n", err)
			return "", err
		}
		u.RawQuery = data.Encode()
		urlStr = u.String()
	}
	return urlStr, nil
}
