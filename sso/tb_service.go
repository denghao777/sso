package sso

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/denghao777/sso/model"
	"gitee.com/denghao777/sso/utils"
	log "github.com/sirupsen/logrus"
	"sync"
)

type TbService struct {
}

const (
	url_login         = "/api/auth/login"
	url_refresh_token = "/api/auth/token"
	url_tenant_infos  = "/api/tenantInfos"
	url_user_info     = "/api/tenant/%s/users"
	url_user_token    = "/api/user/%s/token"
)

var (
	loginMutes   sync.Mutex
	refreshMutes sync.Mutex

	//cache
	tenantTokens sync.Map
	adminToken   model.Token

	//Implementation singleton
	once sync.Once

	tbService *TbService

	tbHost          string
	tbAdminUsername string
	tbAdminPassword string
)

type IotConf struct {
	Url      string
	Username string
	Password string
}

func InitTbService(config *IotConf) error {
	tbHost = config.Url
	tbAdminUsername = config.Username
	tbAdminPassword = config.Password
	var err error
	if tbHost != "" && tbAdminUsername != "" && tbAdminPassword != "" {
		err = login()
	} else {
		err = errors.New("iot config cannot be empty !")
	}
	return err
}

func getTbService() *TbService {
	if tbHost == "" || tbAdminUsername == "" || tbAdminPassword == "" {
		return nil
	}
	once.Do(func() {
		tbService = new(TbService)
	})
	return tbService
}

func (tb *TbService) GetUserToken(tenant string) (*model.Token, error) {
	if tenant == "" {
		log.Error("tenant is empty")
		return nil, errors.New("tenant code cannot be empty")
	}

	if token, ok := tenantTokens.Load(tenant); !ok {
		//query tenant info ,get tenant id
		tenantInfo, err := tb.getTenantInfo(tenant)
		if err != nil {
			return nil, err
		}
		data, err := utils.StrToMap(tenantInfo)
		if err != nil {
			return nil, err
		}
		tenantArray := data["data"].([]interface{})
		if len(tenantArray) == 0 {
			return nil, errors.New("The current tenant was not found : " + tenant)
		}
		tenantId := tenantArray[0].(map[string]interface{})["id"].(map[string]interface{})["id"].(string)

		//query user id by tenant id
		userInfo, err := tb.getUserInfos(tenantId)
		if err != nil {
			return nil, err
		}
		data1, err := utils.StrToMap(userInfo)
		if err != nil {
			return nil, err
		}
		userArray := data1["data"].([]interface{})
		if len(userArray) == 0 {
			return nil, errors.New("The user cannot be found in TB tenant : " + tenant)
		}
		userId := userArray[0].(map[string]interface{})["id"].(map[string]interface{})["id"].(string)

		//get token by userId
		tokenInfo, err := tb.getTokenByUserId(userId)
		if err != nil {
			return nil, err
		}
		var userToken model.Token
		if err := json.Unmarshal([]byte(tokenInfo), &userToken); err != nil {
			return nil, err
		}
		userToken.UserId = userId
		tenantTokens.Store(tenant, userToken)
		return &userToken, nil
	} else {
		result := token.(model.Token)
		t := result.Token
		if ok, err := IsExpiration(t); ok && err == nil {
			log.Infof("TB tenant %s token expired, auto refresh", tenant)
			return tb.RefreshUserToken(tenant)
		}
		return &result, nil
	}
}

func (tb *TbService) RefreshUserToken(tenant string) (*model.Token, error) {
	value, ok := tenantTokens.Load(tenant)
	if !ok {
		log.Warn("no token found, invoke getUserToken() instead of refreshUserToken()")
		return tb.GetUserToken(tenant)
	}
	token := value.(model.Token)

	res, err := refreshToken(token.RefreshToken)
	if err != nil {
		return nil, err
	}

	//parse res
	switch res.StatusCode {
	case 200:
		//str to struct
		var result model.Token
		if err := json.Unmarshal([]byte(res.Body), &result); err != nil {
			return nil, err
		}
		tenantTokens.Store(tenant, result)
		return &result, nil
	case 401:
		tokenInfo, err := tb.getTokenByUserId(token.UserId)
		if err != nil {
			return nil, err
		}
		var userToken model.Token
		if err := json.Unmarshal([]byte(tokenInfo), &userToken); err != nil {
			return nil, err
		}
		userToken.UserId = token.UserId
		tenantTokens.Store(tenant, userToken)
		return &userToken, nil
	default:
		return nil, errors.New("failed to refresh the user token : " + res.Body)
	}

}

// login admin
func login() error {
	loginMutes.Lock()
	defer loginMutes.Unlock()

	//request body
	requestBody, _ := json.Marshal(map[string]string{
		"username": tbAdminUsername,
		"password": tbAdminPassword,
	})

	res, err := httpExecute("POST", &Param{
		Url:  tbHost + url_login,
		Body: string(requestBody),
	})
	if err != nil {
		return err
	}

	if res.StatusCode != 200 {
		return errors.New("Tb admin login failed ")
	}

	if err = json.Unmarshal([]byte(res.Body), &adminToken); err != nil {
		return err
	}
	return nil
}

// refresh admin token
func refreshAdminToken() error {
	refreshMutes.Lock()
	defer refreshMutes.Unlock()
	//map to str
	res, err := refreshToken(adminToken.RefreshToken)
	if err != nil {
		return nil
	}

	//parse res
	switch res.StatusCode {
	case 200:
		//str to struct
		if err := json.Unmarshal([]byte(res.Body), &adminToken); err != nil {
			return err
		}
		return nil
	case 401:
		return login()
	default:
		return errors.New("failed to refresh the admin token")
	}
}

// get tenant info by tenant
func (tb *TbService) getTenantInfo(tenant string) (string, error) {
	return invoke("GET", &Param{
		Url: tbHost + url_tenant_infos,
		Header: map[string]string{
			"X-Authorization": adminToken.GetBearerToken(),
		},
		Query: map[string]string{
			"pageSize":   "10",
			"page":       "0",
			"textSearch": tenant,
		},
	}, httpExecute)
}

// get users info by tenant id
func (tb *TbService) getUserInfos(tenantId string) (string, error) {
	return invoke("GET", &Param{
		Url: tbHost + fmt.Sprintf(url_user_info, tenantId),
		Header: map[string]string{
			"X-Authorization": adminToken.GetBearerToken(),
		},
		Query: map[string]string{
			"pageSize": "10",
			"page":     "0",
		},
	}, httpExecute)
}

// get user token by user id
func (tb *TbService) getTokenByUserId(userId string) (string, error) {
	return invoke("GET", &Param{
		Url: tbHost + fmt.Sprintf(url_user_token, userId),
		Header: map[string]string{
			"X-Authorization": adminToken.GetBearerToken(),
		},
	}, httpExecute)
}

func refreshToken(refreshToken string) (*Res, error) {
	requestBody, _ := json.Marshal(map[string]string{
		"refreshToken": refreshToken,
	})

	//request
	res, err := httpExecute("POST", &Param{
		Url:  tbHost + url_refresh_token,
		Body: string(requestBody),
	})
	if err != nil {
		return nil, err
	}
	return res, nil
}

func invoke(param string,
	param2 *Param,
	request func(httpMethod string, param *Param) (*Res, error)) (string, error) {
	res, err := request(param, param2)
	if err != nil {
		return "", err
	}

	//parse res
	switch res.StatusCode {
	case 200:
		return res.Body, nil
	case 401:
		if err := refreshAdminToken(); err != nil {
			return "", err
		}
		return invoke(param, param2, request)
	default:
		return "", errors.New("remote request failed : " + res.Body)
	}
}
