package sso

import (
	"context"
	"errors"
)

type UserContext struct {
	Username string
	Tenant   string
	Roles    []string
	Token    string
}

func GetUserContext(ctx context.Context) *UserContext {
	return ctx.Value("UserContext").(*UserContext)
}

func (uc *UserContext) GetIotToken() (string, error) {
	tbService := getTbService()
	if tbService == nil {
		return "", errors.New("tb_service is not initialized and cannot provide services !")
	}
	token, err := tbService.GetUserToken(uc.Tenant)
	if err != nil {
		return "", err
	}
	return token.Token, nil
}

func (uc *UserContext) GetIotRefreshToken() (string, error) {
	tbService := getTbService()
	if tbService == nil {
		return "", errors.New("tb_service is not initialized and cannot provide services !")
	}
	token, err := tbService.GetUserToken(uc.Tenant)
	if err != nil {
		return "", err
	}
	return token.RefreshToken, nil
}

func (uc *UserContext) RefreshIotToken() (string, error) {
	tbService := getTbService()
	if tbService == nil {
		return "", errors.New("tb_service is not initialized and cannot provide services !")
	}
	token, err := tbService.RefreshUserToken(uc.Tenant)
	if err != nil {
		return "", err
	}
	return token.Token, nil
}
