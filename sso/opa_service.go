package sso

import (
	"context"
	"errors"
	"gitee.com/denghao777/sso/utils"
	rego "github.com/open-policy-agent/opa/rego"
	"github.com/open-policy-agent/opa/storage/inmem"
	log "github.com/sirupsen/logrus"
	"os"
	"sync"
)

type Opa struct {
	evalQuery *rego.PreparedEvalQuery
	ctx       context.Context
}

type Input struct {
	Method string   `json:"method"`
	Path   string   `json:"path"`
	Roles  []string `json:"roles"`
}

var (
	opa *Opa

	onceOpa sync.Once
)

func GetOpa() *Opa {
	if opa == nil {
		onceOpa.Do(func() {
			if v, err := initOpa(); err != nil {
				panic(err)
			} else {
				opa = v
			}
		})
	}
	return opa
}

func initOpa() (*Opa, error) {
	ctx := context.Background()
	//data
	dataByte, err := os.ReadFile("conf/data.json")
	if err != nil {
		return nil, err
	}
	data, _ := utils.StrToMap(string(dataByte))
	store := inmem.NewFromObject(data)

	//rego
	regoByte, err := os.ReadFile("conf/policy.rego")

	evalQuery, err := rego.New(
		rego.Store(store),
		rego.Query("result = data.httpapi.authz.allow"),
		rego.Module("allow", string(regoByte)),
	).PrepareForEval(ctx)

	if err != nil {
		log.Errorf("failed to init the rego method : %s ", err.Error())
		return nil, err
	}

	return &Opa{
		ctx:       ctx,
		evalQuery: &evalQuery,
	}, nil
}

func (o *Opa) Eval(input Input) (bool, error) {
	evalQuery := o.evalQuery
	if evalQuery == nil {
		return false, errors.New("opa is not initialized !")
	}

	results, err := evalQuery.Eval(o.ctx, rego.EvalInput(input))
	if err != nil {
		return false, err
	}

	return allowed(results), nil
}

func allowed(rs rego.ResultSet) bool {
	if len(rs) == 1 {
		if result, ok := rs[0].Bindings["result"].(bool); ok {
			return result
		}
	}
	log.Errorf("Invalid ResultSet : %v", rs)
	return false
}
