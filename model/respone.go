package model

type Resp struct {
	Code      int32       `json:"code"`
	Message   string      `json:"message"`
	Result    interface{} `json:"result"`
	Timestamp int64       `json:"timestamp"`
}
