package model

import (
	"encoding/base64"
	"gitee.com/denghao777/sso/utils"
	"strings"
)

type Jwt struct {
	Header  map[string]interface{}
	Payload map[string]interface{}
}

func JwtDecode(token string) (*Jwt, error) {
	chunks := strings.Split(token, ".")
	header, err := encoding(chunks[0])
	if err != nil {
		return nil, err
	}

	payload, err := encoding(chunks[1])
	if err != nil {
		return nil, err
	}

	jwt := &Jwt{}
	jwt.Header = header
	jwt.Payload = payload
	return jwt, nil
}

func encoding(param string) (map[string]interface{}, error) {
	var err error
	var paramBytes []byte
	if paramBytes, err = base64.RawURLEncoding.DecodeString(param); err == nil {
		var result map[string]interface{}
		if result, err = utils.StrToMap(string(paramBytes)); err == nil {
			return result, err
		}
	}
	return nil, err
}
