package model

type Token struct {
	UserId       string
	Token        string `json:"token"`
	RefreshToken string `json:"refreshToken"`
}

func (t *Token) GetBearerToken() string {
	return "bearer " + t.Token
}
