package utils

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
)

func StrToMap(str string) (map[string]interface{}, error) {
	var temp = make(map[string]interface{})
	err := json.Unmarshal([]byte(str), &temp)
	if err != nil {
		logrus.Errorf("failed to parse the json str! err msg: %s, data: %s", err, str)
	}
	return temp, err
}
